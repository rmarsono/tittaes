# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Rendy Marsono -- Lead developer -- WebDesignSyd

# THANKS

    <name>

# TECHNOLOGY COLOPHON

    CSS3, HTML5
    Apache Server Configs, jQuery, Modernizr, Normalize.css
    AngularJS

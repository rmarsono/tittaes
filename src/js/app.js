
/** angular app **/
var app = angular.module('Tittaes', []);

/* db */
var girls = [
    { /* girl */
        name: 'jessica rabbit',
        byline: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        rating: 4.5,
        boobs: 'natural 34DD',
        height: 172,
        size: 8,
        hair: 'blonde',
        eyes: 'brown',
        age: 25,
        state: 'nsw',
        city: 'sydney',
        country: 'australia',
        zip: '2000',
        unit: '20',
        street: '4 albion street',
        suburb: 'sydney cbd',
        email: 'jessica@rabbit.com',
        website: 'jessica.rabbit.com',
        phone: '0488 881 881',
        thumb: '../demo/0/ac2685ac7725f85bc3ffdf4998a3ca4b.jpg',
        images: [
            '../demo/0/ac2685ac7725f85bc3ffdf4998a3ca4b.jpg'
        ],
        description: 'Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec ullamcorper nulla non metus auctor fringilla.',
        services: [
            { 'kissing', 0 },
            { 'massage', 0 },
            { 'sex', 0 },
            { 'bbbj', 0 },
            { 'covered bj', 0 },
            { 'cof', 100 },
            { 'cim', 150 },
            { 'gfe', 0 },
            { 'pse', 0 },
            { 'greek', 0 },
            { 'spanish', 0}
        ],
        rates: [
            { 30, 200 },
            { 60, 400 },
            { 120, 700 },
            { 240, 900 }
        ],
        doubles: [
            {
                partner: 'betty boob',
                services: [
                    { 'massage', 0 },
                    { 'bbbj', 0 },
                    { 'covered bj', 0 },
                    { 'cof', 100 },
                    { 'cim', 150 }
                ],
                rates: [
                    { 60, 1000 },
                    { 120, 1500 },
                    { 240, 2000 }
                ]
            },
            {
                partner: 'vampirella',
                services: [
                    { 'cof', 0 },
                    { 'cim', 0 }
                ],
                rates: [
                    { 60, 1100 },
                    { 120, 2000 },
                    { 240, 3000 }
                ]
            }
        ],
        reviews: [
            {
                user: 'user1',
                accurate: true,
                rating: 5,
                review: 'Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Aenean lacinia bibendum nulla sed consectetur. Aenean lacinia bibendum nulla sed consectetur. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. \
Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Curabitur blandit tempus porttitor. \
Etiam porta sem malesuada magna mollis euismod. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum. Curabitur blandit tempus porttitor. \
Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere consectetur est at lobortis. Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo.'
            },
            {
                user: 'user2',
                accurate: true,
                rating: 3.5,
                review: 'Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Donec sed odio dui. Maecenas sed diam eget risus varius blandit sit amet non magna.'
            }
        ]
    } /* end girl */
];